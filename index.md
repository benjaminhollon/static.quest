---

title: The Static Quest
subtitle: Dynamic solutions for static sites
layout: homepage.njk
description: "A collection of dynamic projects to add whimsy to static sites."

---

Static websites—whether hand-crafted or generated—are magic. It's amazing that you can do so much without the benefit of a backend.

That said, when I first moved to a static site generator, I was disappointed to have to remove a few pieces of whimsy spread through my site that just wouldn't work without an ounce of backend code.

This project is my quest to reclaim that whimsy—for me and for you.

<p style="text-align: right">– <a href="https://benjaminhollon.com" target="_blank">Benjamin Hollon</a>, Part-Time Polymath
