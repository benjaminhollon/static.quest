import lume from "lume/mod.ts";
import nunjucks from "lume/plugins/nunjucks.ts";

const markdown = {
	options: {
		breaks: true,
		typographer: true
	}
};

const site = lume({}, { markdown });
site.use(nunjucks());

site.copy("assets");
site.copy(".htaccess");
site.copy(".stfolder");
site.copy("robots.txt");

site.filter(
	"date_iso",
	(date) => {
		return (new Date(date)).toISOString()
	}
);

export default site;
